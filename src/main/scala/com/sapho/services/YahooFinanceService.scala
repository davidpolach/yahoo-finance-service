package com.sapho.services

import java.util.{List => JList}
import java.util.{Map => JMap}

import com.sapho.services.spi.model.Entity
import com.sapho.services.spi.{ServiceMetadata, ConfigurationParameter, Service}
import com.sapho.services.spi.data.ServiceResult
import rapture._
import core._
import io._
import net._
import uri._
import json._
import codec._
import encodings.`UTF-8`._
import jsonBackends.jackson._

class YahooFinanceService extends Service {

  override def getEntities: JList[Entity] = ???

  override def getMetadata: ServiceMetadata = ???

  override def getParameters: JList[ConfigurationParameter] = ???

  override def callService(map: JMap[String, AnyRef]): ServiceResult = {
    val symbol = "SPY"

    val symbolName = Http.parse(s"http://download.finance.yahoo.com/d/quotes.csv?s=$symbol&f=n").slurp[Char]
    println(symbolName)

    val quotes =
      Http.parse(s"http://ichart.yahoo.com/table.csv?s=$symbol&a=0&b=1&c=2015&d=8&e=9&f=2015&ignore=.csv").slurp[Char]
    println(quotes)

    null
  }
}
