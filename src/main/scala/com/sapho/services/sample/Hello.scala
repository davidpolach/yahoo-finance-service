package com.sapho.services.sample

import rapture._
import core._
import io._
import net._
import uri._
import json._
import codec._
import encodings.`UTF-8`._
import jsonBackends.jackson._

case class Member(name: String, born: Int)

case class Group(groupName: String, members: Set[Member])

object Hello {

  def main(args: Array[String]) {
    val src = uri"http://rapture.io/sample.json".slurp[Char]
    val json = Json.parse(src)

    println(json.groups(0).as[Group])
  }
}
